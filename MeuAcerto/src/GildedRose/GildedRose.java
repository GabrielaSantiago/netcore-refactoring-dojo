/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GildedRose;
import java.util.*;

/**
 *
 * @author gabrielaas
 */

public class GildedRose 
{   
    public ArrayList<Item> itens = new ArrayList<Item>();
    
    public GildedRose(ArrayList<Item> Itens) 
    {
        this.itens = Itens;
    }
    
    public void AtualizarQualidade() 
    { 
        for (int i = 0; i < itens.size(); i++) 
        {
            if (!itens.get(i).getNome().equals("Queijo Brie Envelhecido")  && 
                    !itens.get(i).getNome().equals("Ingressos para o concerto do Turisas") &&
                    !itens.get(i).getNome().contains("Conjurado")) 
            {                
                if (itens.get(i).getQualidade() > 0) 
                {
                    if (!itens.get(i).getNome().equals("Dente do Tarrasque")) 
                    {
                        itens.get(i).setQualidade(itens.get(i).getQualidade()-1);
                    }
                }
            }
            else if(itens.get(i).getNome().contains("Conjurado"))
            {   
                if (itens.get(i).getQualidade() > 1) //condição de qualidade até 1 unidade permite decréscimo de qualidade pra itens com qualidade ímpar
                {//decrementa em 2 unidades a qualidade, de acordo com o requisito
                        itens.get(i).setQualidade(itens.get(i).getQualidade()-2);
                        
                }
                else if (itens.get(i).getQualidade() == 1) //caso a qualidade seja ímpar, quando esta chega em 1, decrementa-se apenas 1 unidade, pois, de acordo, com o requisito não se pode haver qualidade negativa
                {
                    itens.get(i).setQualidade(itens.get(i).getQualidade()- 1);
                }
            }
            
            else 
            { 
                if (itens.get(i).getQualidade() < 50) 
                {
                    itens.get(i).setQualidade(itens.get(i).getQualidade()+1);
                    
                    if (itens.get(i).getNome().equals("Ingressos para o concerto do Turisas")) 
                    {
                        if (itens.get(i).getPrazoParaVenda() < 11) 
                        {    
                            if (itens.get(i).getQualidade() < 50) 
                            {
                                itens.get(i).setQualidade(itens.get(i).getQualidade()+1);
                            }   
                        }
                        if (itens.get(i).getPrazoParaVenda() < 6) 
                        {    
                            if (itens.get(i).getQualidade() < 50) 
                            {
                                itens.get(i).setQualidade(itens.get(i).getQualidade()+1);
                            
                            }
                        }
                    }
                }
            }
            
            if (!itens.get(i).getNome().equals("Dente do Tarrasque")) 
            {
                itens.get(i).setPrazoParaVenda(itens.get(i).getPrazoParaVenda()-1);
            }

            if (itens.get(i).getPrazoParaVenda() < 0) 
            {
                if (!itens.get(i).getNome().equals("Queijo Brie Envelhecido")) 
                {
                    if (!itens.get(i).getNome().equals("Ingressos para o concerto do Turisas"))
                    {
                        if (itens.get(i).getQualidade() > 0) 
                        {
                            if (!itens.get(i).getNome().equals("Dente do Tarrasque"))//Não diminui Qualidade porque é item lendário 
                            {
                                itens.get(i).setQualidade(itens.get(i).getQualidade()-1);
                            }
                        }
                    }
                    
                    else 
                    {
                        itens.get(i).setQualidade(itens.get(i).getQualidade()-itens.get(i).getQualidade());
                            //Se o PrazoParaVenda passar, aqui zera-se a qualidade porque não pode ser negativa
                    }
                }
                
                else 
                {
                    if (itens.get(i).getQualidade() < 50) 
                    {
                        itens.get(i).setQualidade(itens.get(i).getQualidade()+1);
                    }
                }
            }
        }
    }
}
