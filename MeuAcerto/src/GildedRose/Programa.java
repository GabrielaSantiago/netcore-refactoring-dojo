/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GildedRose;
import java.util.*;

/**
 *
 * @author gabrielaas
 */

public class Programa 
{
    
    public static void main(String[] args) 
    {
        ArrayList<Item> arrayListaItem = new ArrayList<Item>(); //armazenará os itens
        //inicializa os itens
        arrayListaItem.add(new Item("Corselete +5 DEX",   10,   20));
	arrayListaItem.add(new Item("Queijo Brie Envelhecido",   2,   0));
	arrayListaItem.add(new Item("Elixir do Mangusto",   5,   7));
	arrayListaItem.add(new Item("Dente do Tarrasque",   0,   80));
	arrayListaItem.add(new Item("Dente do Tarrasque",   -1,   80));
        arrayListaItem.add(new Item("Ingressos para o concerto do Turisas",  15,  20));
        arrayListaItem.add(new Item("Ingressos para o concerto do Turisas",  10,  49));
        arrayListaItem.add(new Item("Ingressos para o concerto do Turisas",  5,  49));
        // Este item conjurado agora funciona
        arrayListaItem.add(new Item("Bolo de Mana Conjurado",   3,   6));
     
       
        GildedRose app = new GildedRose(arrayListaItem);
        
        
        for (int i = 0; i < 31; i++) 
        { 
            System.out.println("-------- dia " + i + " --------");
            System.out.println("Nome, PrazoParaVenda, Qualidade");
 
            for (int j = 0; j < arrayListaItem.size(); j++) 
            { 
                System.out.println(arrayListaItem.get(j).getNome() + ", " + arrayListaItem.get(j).getPrazoParaVenda() 
                    + ", " + arrayListaItem.get(j).getQualidade());
            }
            System.out.println("");
            app.AtualizarQualidade();
	}
				
    }
    
}
