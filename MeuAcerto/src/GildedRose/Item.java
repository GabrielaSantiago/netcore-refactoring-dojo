/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GildedRose;

/**
 *
 * @author gabrielaas
 */
public class Item 
{ 
    
    public String Nome;
    public int PrazoParaVenda; 
    public int Qualidade; 

    public Item(String Nome,int PrazoParaVenda, int Qualidade)
    {   
        this.Nome = Nome;
        this.PrazoParaVenda = PrazoParaVenda;
        this.Qualidade = Qualidade;
    
    }

    public String getNome() 
    {
        return Nome;
    }

    public void setNome(String Nome) 
    {
        this.Nome = Nome;
    }

    public int getPrazoParaVenda() 
    {
        return PrazoParaVenda;
    }

    public void setPrazoParaVenda(int PrazoParaVenda) 
    {
        this.PrazoParaVenda = PrazoParaVenda;
    }

    public int getQualidade() 
    {
        return Qualidade;
    }

    public void setQualidade(int Qualidade) 
    {
        this.Qualidade = Qualidade;
    }

    
}
